# Gameguy 2D Scroller

A 2D scroller game built with [Godot Engine 3.1](https://godotengine.org/)

![screenshot](mobile_screenshot.png)

![screenshot2](screenshot2.png)

Built with open source and/or public domain game assets from the following:

Graphics by [http://kenney.nl](http://kenney.nl)

Most sounds by [https://opengameart.org/](https://opengameart.org/)

## Builds

 - Linux, Macos, HTML5, and Android builds in the [Build directory](https://gitlab.com/utopiamachines/gameguy-2d-scroller/tree/master/build) of the repo.
 - Note: They were built using my machine, YMMV

 ## TODO

  - Cross level point scoring [currently scores are per level]
  - Add Save state
  - Add top score screen like arcades of old
  - Add a few more levels and make the current levels longer
  - Add Keys and Locked level doors
  - ~~Add touch controls for mobile gaming~~
  - ~~Export Android builds~~
  - Export IOS builds
  - Much much more...

  Desktop screenshot
  ![screenshot](screenshot.png)